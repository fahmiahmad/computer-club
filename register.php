<!DOCTYPE HTML>
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/style.css">
<link href='http://fonts.googleapis.com/css?family=Crete+Round' rel='stylesheet' type='text/css'>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Lobster|Roboto" rel="stylesheet">
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <title>Jitra Computer Club</title>
 <!--[if lt IE 9]>
 <script
src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
</head>
<body>

<?php

include('header.php');
?>

  <div id="register">
    <div class="wrapper">
      
      <h1 style="text-align: center;">Register</h1>

<br>

<form class="form-horizontal" name="regform" id="regform" method="post" onsubmit="return validateForm();" action="process.php">
<br>Name<br>
<input id="name" type="text" name="name" placeholder="Name" style="margin-left: 0px;">  <span class="err" id="nameErr"></span>
<br><br>IC Number<br>
<input id="ic" type="text" name="ic" placeholder="IC Number" style="margin-left: 0px;"><span class="err" id="icErr"></span>
<br><br>Username<br>
<input id="username" type="text" name="username" placeholder="Username" style="margin-left: 0px;"><span class="err" id="userErr"></span>
<br><br>Password<br>
<input id="password" type="password" name="password" placeholder="Password" style="margin-left: 0px;"><span class="err" id="passErr"></span>
<br><br>Home Address<br>
<textarea id="address" name="address" rows="4" col="5" style="margin-left: 0px;"></textarea><span class="err" id="addressErr"></span>
<br><br>Gender
<p><input id="gender" type="radio" name="gender" value="male">Male
<input id="gender" type="radio" name="gender" value="female">Female
<span class="err" id="genderErr"></span>
<br><br>Email<br>
<input id="email" type="text" name="email" placeholder="Email" style="margin-left: 0px;"><span class="err" id="emailErr"></span>
<br><br>Class<br>
<select id="classname" name="classname">
<option value="" >Select Class
<option value="Opal">Opal
<option value="Amethyst">Amethyst
<option value="Jade">Jade
<option value="Emerald">Emerald
<option value="Sapphire">Sapphire
</select>
<span class="err" id="classErr"></span>
<br><br>Age<br>
<input id="age" type="text" name="age" placeholder="Age" style="margin-left: 0px;"><span class="err" id="ageErr"></span>
<br><br>Telephone<br>
<input id="tel" type="text" name="tel" placeholder="Telephone" style="margin-left: 0px;"><span class="err" id="telErr"></span>
<br><br>Interest<br>
<input id="interest1" type="checkbox" value="true" name="interest1">Computer Games
<input id="interest2" type="checkbox" value="true" name="interest2">Football
<input id="interest3" type="checkbox" value="true" name="interest3">Programming
<input id="interest4" type="checkbox" value="true" name="interest4">Traveling
<input id="interest5" type="checkbox" value="true" name="interest5">Shopping  <span class="err" id="interestErr"></span>
<p>
<br>


<input id="submit" type="submit" name="submit" value="Submit" class="button button1">
<input id="reset" type="reset" name="reset" value="Reset" class="button button1"></p>
</form>
</div>
</div>

<?php
include('footer.php');
?>

</body> 



 </html>

 <script>

function validateForm()
{
    var submit1 = false;
    var submit2 = false;
    var submit3 = false;
    var submit4 = false;
    var submit5 = false;
    var submit6 = false;
    var submit7 = false;
    var submit8 = false;
    var submit9 = false;
    var submit10 = false;
    var submit11 = false;




    var name=document.forms["regform"]["name"].value;
    var ic=document.forms["regform"]["ic"].value;
    var username=document.forms["regform"]["username"].value;
    var password=document.forms["regform"]["password"].value;
    var address=document.forms["regform"]["address"].value;
    var gender=document.forms["regform"]["gender"].value;
    var email=document.forms["regform"]["email"].value;
    var classname =document.forms["regform"]["classname"].value;
    var age=document.forms["regform"]["age"].value;
    var tel=document.forms["regform"]["tel"].value;
    var interest1=document.forms["regform"]["interest1"].checked;
    var interest2=document.forms["regform"]["interest2"].checked;
    var interest3=document.forms["regform"]["interest3"].checked;
    var interest4=document.forms["regform"]["interest4"].checked;
    var interest5=document.forms["regform"]["interest5"].checked;

        if(name == "")
        {
        var nameErr="Name must not be empty!";
        document.getElementById("nameErr").innerHTML = nameErr;
        submit1=false;
        }

        else if(name.length>50)
        {
        var nameErr="Name must be less than 50 characters!";
        document.getElementById("nameErr").innerHTML = nameErr;
        submit1=false;
        }

        else
        {
        var nameErr="";
        document.getElementById("nameErr").innerHTML = nameErr;
        submit1=true;
        }




        if(ic == "")
        {
        var icErr="IC Number must not be empty!";
        document.getElementById("icErr").innerHTML = icErr;
        submit2=false;
        }

        else if(ic.length != 12 || isNaN(ic))
        {
        var icErr="IC Number must be 12 characters and contains only numbers";
        document.getElementById("icErr").innerHTML = icErr;
        submit2=false;
        }
        else 
        {
        var icErr="";
        document.getElementById("icErr").innerHTML = icErr;
        submit2=true;
        }





        if(username == "")
        {
        var userErr="Username must not be empty!";
        document.getElementById("userErr").innerHTML = userErr;
        submit3=false;
        }

        else if(username.length<5 || username.length >12)
        {
        var userErr="Username must be between 5-12 characters!";
        document.getElementById("userErr").innerHTML = userErr;
        submit3=false;
        }

        else 
        {
        var userErr="";
        document.getElementById("userErr").innerHTML = userErr;
        submit3=true;
        }




        if(password == "")
        {
        var passErr="Password must not be empty!";
        document.getElementById("passErr").innerHTML = passErr;
        submit4=false;
        }

        else if(password.length<5 || password.length >12)
        {
        var passErr="Password must be between 5-12 characters!";
        document.getElementById("passErr").innerHTML = passErr;
        submit1=false;
        }

        else 
        {
        var passErr="";
        document.getElementById("passErr").innerHTML = passErr;
        submit4=true;
        }




        if(address == "")
        {
        var addressErr="Home Address must not be empty!";
        document.getElementById("addressErr").innerHTML = addressErr;
        submit5=false;
        }

        else 
        {
        var addressErr="";
        document.getElementById("addressErr").innerHTML = addressErr;
        submit5=true;
        }






        if(gender == "")
        {
        var genderErr="Please select a gender!";
        document.getElementById("genderErr").innerHTML = genderErr;
        submit6=false;
        
        }

        else 
        {
        var genderErr="";
        document.getElementById("genderErr").innerHTML = genderErr;
        submit6=true;
        }



        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;


        if(email == "")
        {
        var emailErr="Email must not be empty!";
        document.getElementById("emailErr").innerHTML = emailErr;
        submit7=false;
        }

        else if (reg.test(email) == false) 
        {
        var emailErr="Email is not valid!";
        document.getElementById("emailErr").innerHTML = emailErr;
        submit7=false;
        }

        else 
        {
        var emailErr= "";
        document.getElementById("emailErr").innerHTML = emailErr;
        submit7=true;
        }






        if(classname== "")
        {
        var classErr="Please select a class!";
        document.getElementById("classErr").innerHTML = classErr;
        submit8=false;
        }

        else 
        {
        var classErr= "";
        document.getElementById("classErr").innerHTML = classErr;
        submit8=true;
        }






        if(age == "")
        {
        var ageErr="Age must not be empty!";
        document.getElementById("ageErr").innerHTML = ageErr;
        submit9=false;
        }

        else if(isNaN(age))
        {
        var ageErr="Age must contains only numbers";
        document.getElementById("ageErr").innerHTML = ageErr;
        submit9=false;
        }
        else 
        {
        var ageErr="";
        document.getElementById("ageErr").innerHTML = ageErr;
        submit9=true;
        }





         if(tel == "")
        {
        var telErr="Telephone must not be empty!";
        document.getElementById("telErr").innerHTML = telErr;
        submit10=false;
        }

        else if(isNaN(tel))
        {
        var telErr="Telephone must contains only numbers";
        document.getElementById("telErr").innerHTML = telErr;
        submit10=false;
        }
        else 
        {
        var telErr="";
        document.getElementById("telErr").innerHTML = telErr;
        submit10=true;
        }







         if(!interest1 && !interest2 && !interest3 && !interest4 && !interest5)
        {
        var interestErr="Please select at least one interest!";
        document.getElementById("interestErr").innerHTML = interestErr;
        submit11=false;
        }


        else 
        {
        var interestErr= "";
        document.getElementById("interestErr").innerHTML = interestErr;
        submit11=true;
        }


    

if((!submit1) || (!submit2) || (!submit3) || (!submit4) || (!submit5) || (!submit6) || (!submit7) || (!submit8) || (!submit9) || (!submit10) || (!submit11) )
    {
        return false;
        
    }

    else
    {
        return true;
    }
        





    
        


}




</script>