<?php

include('header.php');
?>

<style>
*,
*::before,
*::after {
 margin: 0;
 padding: 0;
 box-sizing: inherit;
}

body {
 font: 100%/1.5 -apple-system, BlinkMacSystemFont, "Segoe UI", "Lucida Grande", Calibri, Verdana, sans-serif;
 box-sizing: border-box;
}

.mySlides {
 display:none;
}

img {
 max-width: 100%;
 margin-bottom: -5px;
}

figure {
 text-align:center;
} 

.w3-col {
 float: left;
}

.s4 {
  width: 33.33333%;
}

.w3-content {
  max-width: 980px;
  margin: auto;
}


.w3-container {
  padding: 0.01em 16px;
}

.w3-container:after,.w3-panel:after,.w3-row:after,.w3-row-padding:after,.w3-topnav:after,.w3-clear:after,.w3-btn-group:before,.w3-btn-group:after,.w3-btn-bar:before,.w3-btn-bar:after {
  content: "";
  display: table;
  clear: both;
}

.w3-row-padding {
  padding: 0 8px;
}

.w3-row-padding > .w3-col {
  padding: 0 8px;
}

.w3-section {
  margin-top: 16px !important;
  margin-bottom: 16px !important;
}

[class*="w3-border-"] {
 border-style: solid;
 border-width: 1px; 
}

.w3-border-red,
.w3-hover-border-red:hover {
  border-color: #f44336 !important;
}

.w3-hover-shadow {
transition: background-color .3s,color .15s,box-shadow .3s,opacity 0.3s;
}

.w3-hover-shadow:hover {
 box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19)!important;
}

 pre {
 font-family: Consolas, Menlo, monospace;
 font-size: 1em;
 white-space: pre-wrap;
}
</style>

<div>
<h2>SEKOLAH MENENGAH KEBANGSAAN POKOK SENA</h2>
</div>

<div class="w3-content" style="max-width:1200px">
  <figure class="mySlides" >
    <img src="images/smkps3.jpg" style="width:50%">
    <figcaption>
      <pre>Entrance</pre>
    </figcaption>
  </figure>
  <figure class="mySlides">
    <img src="images/smkps2.jpg" style="width:50%">
    <figcaption>
      <pre>Iconic Mountain</pre>
    </figcaption>
  </figure>
  <figure class="mySlides">
    <img src="images/smkps4.jpg" style="width:50%">
    <figcaption>
      <pre>Hall</pre>
    </figcaption>
  </figure>  

  <div class="w3-row-padding w3-section">
    <div class="w3-col s4">
      <img class="demo w3-border w3-hover-shadow" src="images/smkps3.jpg" style="width:100%; height:160px;" onclick="currentDiv(1)">
    </div>
    <div class="w3-col s4">
      <img class="demo w3-border w3-hover-shadow" src="images/smkps2.jpg" style="width:100%; height:160px;" onclick="currentDiv(2)">
    </div>
    <div class="w3-col s4">
      <img class="demo w3-border w3-hover-shadow" src="images/smkps4.jpg" style="width:100%; height:160px;" onclick="currentDiv(3)">
    </div>
  </div>
</div>

<script>
var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function currentDiv(n) {
  showDivs(slideIndex = n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  if (n > x.length) {slideIndex = 1}
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
     dots[i].className = dots[i].className.replace(" w3-border-red", "");
  }
  x[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " w3-border-red";
}
</script>


<div id="primary-content" style="background-image: url('images/smkps2.jpg'); width: 70%; margin-left: 200px; ">  

	<div class="wrapper">   
		<article>    
			<h3>Club Information</h3>    

			<p style="color: black">The computer club of SMK Pokok Sena was found in 2007.</p>   

			   

			</article>  
		</div> 
	</div>



<div id="secondary-content"> 	
	<div class="wrapper">   

		<article>    

			<div class="overlay">     
				<h4>Upcoming Events</h4>     

				<ol>
					<li> Multimedia Competition </li>
					<li> Web Development Camp </li>
					<li> Computer Club Week </li>
				</ol>


			</div>   
		</article>   

		<article>    

			<div class="overlay">     
				<h4>Announcement</h4>

        <p>UPCOMING MEETING!</p>
        <ul style="margin-left: 20px;">
          <li>VENUE : LAB 1</li>
          <li>TIME : 2.00</li>
        </ul>    

				
  

			</div>

		</article>

			<div class="clear"></div>  

	</div> 
</div>







<?php

include('footer.php');
?>




</body> 


 </html>